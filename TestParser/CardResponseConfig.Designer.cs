﻿namespace TestParser
{
    partial class CardResponseConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CardsList = new System.Windows.Forms.ListBox();
            this.panBox = new System.Windows.Forms.TextBox();
            this.rcBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.newB = new System.Windows.Forms.Button();
            this.edit = new System.Windows.Forms.Button();
            this.deleteB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CardsList
            // 
            this.CardsList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.CardsList.FormattingEnabled = true;
            this.CardsList.Location = new System.Drawing.Point(12, 12);
            this.CardsList.Name = "CardsList";
            this.CardsList.Size = new System.Drawing.Size(158, 212);
            this.CardsList.TabIndex = 9;
            this.CardsList.SelectedIndexChanged += new System.EventHandler(this.MessagesList_SelectedIndexChanged);
            // 
            // panBox
            // 
            this.panBox.Location = new System.Drawing.Point(177, 13);
            this.panBox.Name = "panBox";
            this.panBox.Size = new System.Drawing.Size(192, 20);
            this.panBox.TabIndex = 10;
            // 
            // rcBox
            // 
            this.rcBox.Location = new System.Drawing.Point(278, 42);
            this.rcBox.Name = "rcBox";
            this.rcBox.Size = new System.Drawing.Size(91, 20);
            this.rcBox.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Response Code:";
            // 
            // newB
            // 
            this.newB.Location = new System.Drawing.Point(233, 68);
            this.newB.Name = "newB";
            this.newB.Size = new System.Drawing.Size(64, 23);
            this.newB.TabIndex = 13;
            this.newB.Text = "Add";
            this.newB.UseVisualStyleBackColor = true;
            this.newB.Click += new System.EventHandler(this.newB_Click);
            // 
            // edit
            // 
            this.edit.Location = new System.Drawing.Point(176, 68);
            this.edit.Name = "edit";
            this.edit.Size = new System.Drawing.Size(51, 23);
            this.edit.TabIndex = 14;
            this.edit.Text = "Edit";
            this.edit.UseVisualStyleBackColor = true;
            this.edit.Click += new System.EventHandler(this.edit_Click);
            // 
            // deleteB
            // 
            this.deleteB.Location = new System.Drawing.Point(303, 68);
            this.deleteB.Name = "deleteB";
            this.deleteB.Size = new System.Drawing.Size(66, 23);
            this.deleteB.TabIndex = 15;
            this.deleteB.Text = "Delete";
            this.deleteB.UseVisualStyleBackColor = true;
            this.deleteB.Click += new System.EventHandler(this.deleteB_Click);
            // 
            // CardResponseConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 261);
            this.Controls.Add(this.deleteB);
            this.Controls.Add(this.edit);
            this.Controls.Add(this.newB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rcBox);
            this.Controls.Add(this.panBox);
            this.Controls.Add(this.CardsList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CardResponseConfig";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CardResponseConfig";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox CardsList;
        private System.Windows.Forms.TextBox panBox;
        private System.Windows.Forms.TextBox rcBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button newB;
        private System.Windows.Forms.Button edit;
        private System.Windows.Forms.Button deleteB;
    }
}