﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace TestParser
{
    public partial class CardResponseConfig : Form
    {
        List<CreditCard> Cards;
        Configuration config;
        KeyValueConfigurationCollection rcs;

        public CardResponseConfig()
        {
            InitializeComponent();
            Cards = new List<CreditCard>();
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            rcs = ((AppSettingsSection)config.GetSection("ResponseCodes")).Settings;

            foreach (var c in rcs.AllKeys)
            {
                Cards.Add(new CreditCard() { Pan = c, ResponseCode = rcs[c].Value });
            }
            UpdateCards();
        }

        private void newB_Click(object sender, EventArgs e)
        {
            if (Cards.Exists(c => c.Pan == panBox.Text)) return;
            if (rcBox.Text.Length != 2) return;
            if (panBox.Text.Length < 13 || panBox.Text.Length > 19) return;

            Cards.Add(new CreditCard() { Pan = panBox.Text, ResponseCode = rcBox.Text });
            rcs.Add(panBox.Text, rcBox.Text);
            SaveConfig();
            UpdateCards();
        }

        private void edit_Click(object sender, EventArgs e)
        {
            if (!Cards.Exists(c => c.Pan == panBox.Text)) return;
            if (rcBox.Text.Length != 2) return;
            if (panBox.Text.Length < 13 || panBox.Text.Length > 19) return;

            Cards.Find(c => c.Pan == panBox.Text).ResponseCode = rcBox.Text;
            rcs.Remove(panBox.Text);
            rcs.Add(panBox.Text, rcBox.Text);
            SaveConfig();
            UpdateCards();
        }

        private void deleteB_Click(object sender, EventArgs e)
        {
            if (!Cards.Exists(c => c.Pan == panBox.Text)) return;

            Cards.Remove(Cards.Find(c => c.Pan == panBox.Text));
            rcs.Remove(panBox.Text);
            SaveConfig();
            UpdateCards();
        }

        public void SaveConfig()
        {
            try
            {
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
            }
            catch { }
        }

        private void MessagesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = CardsList.SelectedIndex;
            if (i < 0) return;

            panBox.Text = Cards[i].Pan;
            rcBox.Text = Cards[i].ResponseCode;
        }

        private void UpdateCards()
        {
            CardsList.Items.Clear();
            foreach (var c in Cards)
            {
                CardsList.Items.Add(c.Pan);
            }
        }
    }
}
