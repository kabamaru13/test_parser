﻿namespace TestParser
{
    class ApiData
    {
        public string BaseUrl { get; set; }
        public string OrderUrl { get; set; }
        public string TransactionUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public long Amount { get; set; }
        public long OrderCode { get; set; }
        public string CardNumber { get; set; }
        public string ExpDate { get; set; }
        public string Cvv { get; set; }
        public string Cavv { get; set; }
        public string TransactionId { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorText { get; set; }

        public ApiData()
        {

        }
    }
}
