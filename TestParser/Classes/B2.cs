﻿namespace TestParser
{
    class B2 : Token
    {
        public string BIT_MAP { get; set; } // p1-4
        public string USER_FLD1 { get; set; } // p5-8
        public string CRYPTO_INFO_DATA { get; set; } // p9-10
        public string TVR { get; set; } // p11-20
        public string ARQC { get; set; } // p21-36
        public string AMT_AUTH { get; set; } // p37-48
        public string AMT_OTHER { get; set; } // p49-60
        public string AIP { get; set; } // p61-64
        public string ATC { get; set; } // p65-68
        public string TERM_CNTRY_CDE { get; set; } // p69-71
        public string TRAN_CRNCY_CDE { get; set; } // p72-74
        public string TRAN_DAT { get; set; } // p75-80
        public string TRAN_TYPE { get; set; } // p81-82
        public string UNPREDICT_NUM { get; set; } // p83-90
        public string ISS_APPL_DATA_LGTH { get; set; } // p91-94
        public string ISS_APPL_DATA { get; set; } // p95-158

        public B2(string h, string n, string d) : base(h, n, d)
        {
            BIT_MAP = d.Substring(0, 4);
            USER_FLD1 = d.Substring(4, 4);
            CRYPTO_INFO_DATA = d.Substring(8, 2);
            TVR = d.Substring(10, 10);
            ARQC = d.Substring(20, 16);
            AMT_AUTH = d.Substring(36, 12);
            AMT_OTHER = d.Substring(48, 12);
            AIP = d.Substring(60, 4);
            ATC = d.Substring(64, 4);
            TERM_CNTRY_CDE = d.Substring(68, 3);
            TRAN_CRNCY_CDE = d.Substring(71, 3);
            TRAN_DAT = d.Substring(74, 6);
            TRAN_TYPE = d.Substring(80, 2);
            UNPREDICT_NUM = d.Substring(82, 8);
            ISS_APPL_DATA_LGTH = d.Substring(90, 4);
            ISS_APPL_DATA = d.Substring(94, 64);
        }
    }
}
