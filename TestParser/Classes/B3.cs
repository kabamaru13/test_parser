﻿namespace TestParser
{
    class B3 : Token
    {
        public string BIT_MAP { get; set; } // p1-4
        public string TERM_SERL_NUM { get; set; } // p5-12
        public string EMV_TERM_CAP { get; set; } // p13-20
        public string USER_FLD1 { get; set; } // p21-24
        public string USER_FLD2 { get; set; } // p25-32
        public string EMV_TERM_TYPE { get; set; } // p33-34
        public string APPL_VER_NUM { get; set; } // p35-38
        public string CVM_RSLTS { get; set; } // p39-44
        public string DF_NAME_LGTH { get; set; } // p45-48
        public string DF_NAME { get; set; } // p49-80

        public B3(string h, string n, string d) : base(h, n, d)
        {
            BIT_MAP = d.Substring(0, 4);
            TERM_SERL_NUM = d.Substring(4, 8);
            EMV_TERM_CAP = d.Substring(12, 8);
            USER_FLD1 = d.Substring(20, 4);
            USER_FLD2 = d.Substring(24, 8);
            EMV_TERM_TYPE = d.Substring(32, 2);
            APPL_VER_NUM = d.Substring(34, 4);
            CVM_RSLTS = d.Substring(38, 6);
            DF_NAME_LGTH = d.Substring(44, 4);
            DF_NAME = d.Substring(48, 32);
        }
    }
}
