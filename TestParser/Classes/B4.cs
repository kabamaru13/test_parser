﻿namespace TestParser
{
    class B4 : Token
    {
        public string PT_SRV_ENTRY_MDE { get; set; } // p1-3
        public string TERM_ENTRY_CAP { get; set; } // p4
        public string LAST_EMV_STAT { get; set; } // p5
        public string DATA_SUSPECT { get; set; } // p6
        public string APPL_PAN_SEQ_NUM { get; set; } // p7-8
        public string CVM_RSLTS { get; set; } // p9-14
        public string RSN_ONL_CDE { get; set; } // p15-18
        public string ARQC_VRFY { get; set; } // p19
        public string ISO_RC_IND { get; set; } // p20

        public B4(string h, string n, string d) : base(h, n, d)
        {
            PT_SRV_ENTRY_MDE = d.Substring(0, 3);
            TERM_ENTRY_CAP = d.Substring(3, 1);
            LAST_EMV_STAT = d.Substring(4, 1);
            DATA_SUSPECT = d.Substring(5, 1);
            APPL_PAN_SEQ_NUM = d.Substring(6, 2);
            CVM_RSLTS = d.Substring(8, 6);
            RSN_ONL_CDE = d.Substring(14, 4);
            ARQC_VRFY = d.Substring(18, 1);
            ISO_RC_IND = d.Substring(19, 1);
        }
    }
}
