﻿namespace TestParser
{
    class BY : Token
    {
        public string STAN { get; set; } // p1-6
        public string RETRVL_REF_NUM { get; set; } // p7-18
        public string NETWK_ID { get; set; } // p19-22
        public string RESP_CDE { get; set; } // p23-25
        public string ISA_IND { get; set; } // p26
        public string ISA_AMT { get; set; } // p27-34
        public string FEE_PGM_IND { get; set; } // p35-37
        public string ON_OFF_PREM_IND { get; set; } // p38
        public string CROSS_BORDER_TXN_IND { get; set; } // p39
        public string CROSS_BORDER_CRNCY_IND { get; set; } // p40
        public string CRNCY_CONV_ASSESS_AMT { get; set; } // p41-52
        public string CRD_LVL_PROD_ID_VAL { get; set; } // p53-54
        public string USER_FLD_ACI { get; set; } // p55-60

        public BY(string h, string n, string d) : base(h, n, d)
        {
            STAN = d.Substring(0, 6);
            RETRVL_REF_NUM = d.Substring(6, 12);
            NETWK_ID = d.Substring(18, 4);
            RESP_CDE = d.Substring(22, 3);
            ISA_IND = d.Substring(25, 1);
            ISA_AMT = d.Substring(26, 8);
            FEE_PGM_IND = d.Substring(34, 3);
            ON_OFF_PREM_IND = d.Substring(37, 1);
            CROSS_BORDER_TXN_IND = d.Substring(38, 1);
            CROSS_BORDER_CRNCY_IND = d.Substring(39, 1);
            CRNCY_CONV_ASSESS_AMT = d.Substring(40, 12);
            CRD_LVL_PROD_ID_VAL = d.Substring(52, 2);
            USER_FLD_ACI = d.Substring(54, 6);
        }
    }
}
