﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace TestParser
{
    class BicIsoMessage : IsoMessage
    {
        int[] l1 = { 27, 66, 91 };
        int[] l2 = { 25, 26, 39, 67, 92 };
        int[] l3 = { 19, 20, 21, 22, 23, 24, 40, 49, 50, 51, 68, 69, 70 };
        int[] l4 = { 13, 14, 15, 16, 17, 18, 71, 72 };
        int[] l5 = { 93 };
        int[] l6 = { 3, 11, 12, 38, 73 };
        int[] l7 = { 94 };
        int[] l8 = { 8, 9, 10 };
        int[] l10 = { 7, 74, 75, 76, 77, 78, 79, 80, 81 };
        int[] l12 = { 4, 5, 6, 37, 82, 83, 84, 85 };
        int[] l15 = { 42 };
        int[] l16 = { 1, 41, 52, 53, 64, 86, 87, 88, 89, 96, 128 };
        int[] l25 = { 98 };
        int[] l40 = { 43 };
        int[] l42 = { 90, 95 };
        int[] lvar2 = { 2, 28, 29, 30, 31, 32, 33, 34, 35, 44, 45, 97, 99, 100, 101, 102, 103 };
        int[] lvar3 = { 36, 46, 47, 48, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127 };

        int[] mandatory = { 1, 3, 4, 7, 11, 12, 13, 17, 18, 22, 32, 35, 37, 41, 43, 48, 49, 60, 61, 63, 100, 121, 124, 125, 126 };

        public bool IsPreAuth { get; set; }
        public bool IsCP { get; set; }
        public bool IsMpos { get; set; }
        public bool IsContactless { get; set; }
        public bool IsRecurring { get; set; }
        public bool IsMoto { get; set; }
        public bool IsRefund { get; set; }
        public bool IsVoid { get; set; }
        public bool IsReversal { get; set; }
        public bool IsNetwork { get; set; }

        public List<Token> Tokens { get; set; }

        public BicIsoMessage()
        {
            PrimaryDEs = new string[64];
            SecondaryDEs = new string[64];
            Tokens = new List<Token>();
            Created = DateTime.Now;
            Type = 3;
        }

        public BicIsoMessage(string msg)
        {
            PrimaryDEs = new string[64];
            SecondaryDEs = new string[64];
            Tokens = new List<Token>();
            Created = DateTime.Now;
            Type = 3;

            if (string.IsNullOrWhiteSpace(msg))
            {
                Errors = "Message was empty!";
                return;
            }

            if (msg.StartsWith("ISO")) // msg contains Header
            {
                Header = msg.Substring(0, 12);
                msg = msg.Substring(12);
            }

            if (msg.Length < 20)
            {
                Errors = "Message length was smaller than 20 characters!";
                return;
            }

            MTI = msg.Substring(0, 4);
            if (int.Parse(MTI[2].ToString()) % 2 == 0) IsRequest = true;
            else IsRequest = false;
            PrimaryBitmap = msg.Substring(4, 16);

            if (msg.Length == 20)
            {
                Warnings += "! Message contained only the primary bitmap!" + Environment.NewLine;
                return;
            }
            msg = msg.Substring(20); // message left are the DEs

            BitArray bits = HandleBitmap(PrimaryBitmap);

            int i = 0;
            try
            {
                foreach (bool bit in bits)
                {
                    if (bit)
                    {
                        PrimaryDEs[i] = msg.Substring(0, GetDERule(i + 1, msg));
                        msg = msg.Substring(GetDERule(i + 1, msg));
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                Errors = "Error on data element " + (i + 1) + "." + Environment.NewLine;
                Errors += ex.Message;
                return;
            }

            try
            {
                i = 0;
                bits = HandleBitmap(PrimaryDEs[0]);
                foreach (bool bit in bits)
                {
                    if (bit)
                    {
                        SecondaryDEs[i] = msg.Substring(0, GetDERule(i + 65, msg));
                        msg = msg.Substring(GetDERule(i + 65, msg));
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                Errors = "Error on data element " + (i + 65) + "." + Environment.NewLine;
                Errors += ex.Message;
                return;
            }

            ParseTokens();

            ExecuteValidations();
        }

        public new string PrintIso()
        {
            string disp = base.PrintIso();

            if (Tokens.Count > 0)
            {
                foreach (var token in Tokens)
                {
                    disp += "- Token " + token.Name + " Header: " + token.Header + Environment.NewLine;
                    disp += "- Token " + token.Name + " Data: " + token.Data + Environment.NewLine;
                }

                disp += Environment.NewLine;
            }

            return disp;
        }

        public void ParseTokens()
        {
            if (string.IsNullOrWhiteSpace(PrimaryDEs[62]))
            {
                if (!MTI.StartsWith("08")) Warnings += "! No tokens were present in the message!" + Environment.NewLine;
                return;
            }

            string de = PrimaryDEs[62];
            de = de.Substring(3);
            // handle header token as token without data for presentation reasons
            if (!de.StartsWith("&"))
            {
                Errors = "Header Token did not start with '&' character!";
                return;
            }
            Tokens.Add(new Token(de.Substring(0, 12), "", ""));
            de = de.Substring(12);

            // handle rest of the tokens
            try
            {
                while (de.Length > 0)
                {
                    if (!de.StartsWith("!"))
                    {
                        Errors = "Token header did not start with '!' character!";
                        return;
                    }

                    string h = de.Substring(0, 10);
                    string n = de.Substring(2, 2);
                    string l = de.Substring(4, 5);
                    int len = 0;
                    int.TryParse(l, out len);
                    string d = de.Substring(10, len);
                    if (n == "04" && len == 20) Tokens.Add(new _04(h, n, d));
                    else if (n == "B2" && len == 158) Tokens.Add(new B2(h, n, d));
                    else if (n == "B3" && len == 80) Tokens.Add(new B3(h, n, d));
                    else if (n == "B4" && len == 20) Tokens.Add(new B4(h, n, d));
                    else if (n == "BY" && len == 60) Tokens.Add(new BY(h, n, d));
                    else if (n == "C0" && len == 26) Tokens.Add(new C0(h, n, d));
                    else if (n == "C4" && len == 12) Tokens.Add(new C4(h, n, d));
                    else if (n == "CH" && len == 40) Tokens.Add(new CH(h, n, d));
                    else if (n == "P3" && len == 80) Tokens.Add(new P3(h, n, d));
                    else if (n == "S4" && len == 166) Tokens.Add(new S4(h, n, d));
                    else Tokens.Add(new Token(h, n, d));

                    len += 10;
                    if (de.Length > len) de = de.Substring(len);
                    else de = "";
                }
            }
            catch (Exception ex)
            {
                Errors = "Error on token parsing." + Environment.NewLine;
                Errors += ex.Message;
                return;
            }
        }

        public void ExecuteValidations()
        {
            if (!IsRequest) return;
            if (MTI.StartsWith("08")) // Network Message - Other Validations
            {
                IsNetwork = true;
                if (string.IsNullOrWhiteSpace(PrimaryDEs[0])) Warnings += "! Mandatory Data Element [1] was not present in the message!" + Environment.NewLine;
                if (string.IsNullOrWhiteSpace(PrimaryDEs[6])) Warnings += "! Mandatory Data Element [7] was not present in the message!" + Environment.NewLine;
                if (string.IsNullOrWhiteSpace(PrimaryDEs[10])) Warnings += "! Mandatory Data Element [11] was not present in the message!" + Environment.NewLine;
                if (string.IsNullOrWhiteSpace(SecondaryDEs[5])) Warnings += "! Mandatory Data Element [70] was not present in the message!" + Environment.NewLine;
                return;
            }
            #region Mandatory DataElements
            for (int i = 0; i < 64; i++)
            {
                if (mandatory.Contains(i + 1) && string.IsNullOrWhiteSpace(PrimaryDEs[i])) Warnings += "! Mandatory Data Element [" + (i + 1) + "] was not present in the message!" + Environment.NewLine;
            }
            for (int i = 0; i < 64; i++)
            {
                if (mandatory.Contains(i + 65) && string.IsNullOrWhiteSpace(SecondaryDEs[i])) Warnings += "! Mandatory Data Element [" + (i + 65) + "] was not present in the message!" + Environment.NewLine;
            }
            _04 t04 = Tokens.Find(x => x.Name == "04") as _04;
            B2 tB2 = Tokens.Find(x => x.Name == "B2") as B2;
            B3 tB3 = Tokens.Find(x => x.Name == "B3") as B3;
            B4 tB4 = Tokens.Find(x => x.Name == "B4") as B4;
            BY tBY = Tokens.Find(x => x.Name == "BY") as BY;
            C0 tC0 = Tokens.Find(x => x.Name == "C0") as C0;
            C4 tC4 = Tokens.Find(x => x.Name == "C4") as C4;
            CH tCH = Tokens.Find(x => x.Name == "CH") as CH;
            P3 tP3 = Tokens.Find(x => x.Name == "P3") as P3;
            S4 tS4 = Tokens.Find(x => x.Name == "S4") as S4;
            if (tB4 == null) Warnings += "! Token B4 not found." + Environment.NewLine;
            if (tC0 == null) Warnings += "! Token C0 not found." + Environment.NewLine;
            if (tC4 == null) Warnings += "! Token C4 not found." + Environment.NewLine;
            if (tP3 == null) Warnings += "! Token P3 not found." + Environment.NewLine;
            if (t04 == null) Warnings += "! Token 04 not found." + Environment.NewLine;
            #endregion

            #region Transaction Types
            IsPreAuth = false;
            IsCP = false;
            IsMpos = false;
            IsContactless = false;
            IsRecurring = false;
            IsMoto = false;
            IsRefund = false;
            IsVoid = false;
            IsReversal = false;
            IsNetwork = false;

            if (MTI == "0100") IsPreAuth = true;
            if (MTI == "0420") IsReversal = true;
            if (PrimaryDEs[21] != "012")
            {
                IsCP = true; // POSEntry of E-Commerce, MoTo, Recurring == 012
                if (PrimaryDEs[21].StartsWith("07")) IsContactless = true;
            }
            if (PrimaryDEs[2].StartsWith("80")) IsMoto = true;
            if (PrimaryDEs[2].StartsWith("20")) IsRefund = true;
            if (PrimaryDEs[2].StartsWith("22")) IsVoid = true;
            if (tCH != null)
            {
                if (tCH.EMV_CAPABLE_OUTLET == "Y") IsMpos = true;
                if (tCH.RECUR_PMNT_IND == "R") IsRecurring = true;
            }

            #region PreAuth
            if (IsPreAuth)
            {
                try
                {
                    if (tC4 != null)
                    {
                        if (tC4.TXN_STAT_IND != "4") Warnings += "! TXN-STAT-IND on Token C4 should be 4." + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    Warnings += "! Error on PreAuth Validations." + Environment.NewLine;
                    Warnings += ex.Message + Environment.NewLine;
                }
            }
            else
            {
                try
                {
                    if (tC4 != null)
                    {
                        if (tC4.TXN_STAT_IND != "0") Warnings += "! TXN-STAT-IND on Token C4 should be 0." + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    Warnings += "! Error on PreAuth Validations." + Environment.NewLine;
                    Warnings += ex.Message + Environment.NewLine;
                }
            }
            #endregion

            #region Card Present
            if (IsCP)
            {
                try
                {
                    if (IsMoto) Warnings += "! The CP transaction was formatted as MoTo." + Environment.NewLine;
                    if (IsRecurring) Warnings += "! The CP transaction was formatted as Recurring." + Environment.NewLine;
                    if (IsRefund) Warnings += "! The CP transaction was formatted as Refund." + Environment.NewLine;
                    if (IsVoid) Warnings += "! The CP transaction was formatted as Void." + Environment.NewLine;
                    if (IsReversal) Warnings += "! The CP transaction was formatted as Reversal." + Environment.NewLine;

                    if (t04 != null)
                    {
                        if (t04.COMPLETE_TRACK2_DATA != "Y") Warnings += "! COMPLETE-TRACK2-DATA on Token 04 should be Y." + Environment.NewLine;
                    }

                    if (tC0 != null)
                    {
                        if (tC0.E_COM_FLG != " ") Warnings += "! E-COM-FLG on Token C0 should be ' '." + Environment.NewLine;
                    }

                    if (tC4 != null)
                    {
                        if (tC4.TERM_ATTEND_IND != "0") Warnings += "! TERM-ATTEND-IND on Token C4 should be 0." + Environment.NewLine;
                        if (tC4.TERM_LOC_IND != "0") Warnings += "! TERM-LOC-IND on Token C4 should be 0." + Environment.NewLine;
                        if (tC4.CRDHLDR_PRESENT_IND != "0") Warnings += "! CRDHLDR-PRESENT-IND on Token C4 should be 0." + Environment.NewLine;
                        if (tC4.CRD_PRESENT_IND != "0") Warnings += "! CRD-PRESENT-IND on Token C4 should be 0." + Environment.NewLine;
                        if (tC4.CRDHLDR_ACTVT_TERM_IND != "0") Warnings += "! CRDHLDR-ACTVT-TERM-IND on Token C4 should be 0." + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    Warnings += "! Error on CP Validations." + Environment.NewLine;
                    Warnings += ex.Message + Environment.NewLine;
                }
            }
            #endregion

            #region mPOS
            if (IsMpos)
            {
                try
                {
                    if (tCH != null)
                    {
                        if (!(tCH.RECUR_PMNT_IND == "X" || tCH.RECUR_PMNT_IND == " ")) Warnings += "! RECUR-PMNT-IND on Token CH should be 'X' or ' '." + Environment.NewLine;
                        if (tCH.TERM_TYP != "9") Warnings += "! TERM-TYP on Token CH should be 9." + Environment.NewLine;
                        if (tCH.PMNT_INFO != " ") Warnings += "! PMNT-INFO on Token CH should be ' '." + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    Warnings += "! Error on mPOS Validations." + Environment.NewLine;
                    Warnings += ex.Message + Environment.NewLine;
                }
            }
            #endregion

            #region Contactless
            if (IsContactless)
            {
                try
                {
                    if (tS4 == null) Warnings += "! Token S4 not found." + Environment.NewLine;

                    if (tS4 != null)
                    {
                        if (tS4.DATASET_ID != "01") Warnings += "! DATASET-ID on Token S4 should be 01." + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    Warnings += "! Error on Contactless Validations." + Environment.NewLine;
                    Warnings += ex.Message + Environment.NewLine;
                }
            }
            #endregion

            #region Card not Present
            if (!IsCP)
            {
                try
                {
                    if (PrimaryDEs[21] != "012") Warnings += "! Data Element [22] was not '012'." + Environment.NewLine;
                    if (IsContactless) Warnings += "! The CnP transaction was formatted as Contactless." + Environment.NewLine;
                    if (IsMpos) Warnings += "! The CnP transaction was formatted as mPOS." + Environment.NewLine;

                    if (t04 != null)
                    {
                        if (t04.COMPLETE_TRACK2_DATA != "N") Warnings += "! CVD-FLD-PRESENT on Token C0 should be 0." + Environment.NewLine;
                    }

                    if (tB4 != null)
                    {
                        if (tB4.TERM_ENTRY_CAP != "0") Warnings += "! TERM-ENTRY-CAP on Token B4 should be 0." + Environment.NewLine;
                        if (tB4.LAST_EMV_STAT != " ") Warnings += "! LAST-EMV-STAT on Token B4 should be ' '." + Environment.NewLine;
                    }

                    if (tC0 != null)
                    {
                        if (!IsMoto && tC0.E_COM_FLG != "7") Warnings += "! E-COM-FLG on Token C0 should be 7." + Environment.NewLine;
                    }

                    if (tC4 != null)
                    {
                        if (!IsMoto && tC4.TERM_ATTEND_IND != "1") Warnings += "! TERM-ATTEND-IND on Token C4 should be 1." + Environment.NewLine;
                        if (!IsMoto && tC4.TERM_LOC_IND != "2") Warnings += "! TERM-LOC-IND on Token C4 should be 2." + Environment.NewLine;
                        if (!IsRecurring && !IsMoto && tC4.CRDHLDR_PRESENT_IND != "5") Warnings += "! CRDHLDR-PRESENT-IND on Token C4 should be 5." + Environment.NewLine;
                        if (!IsMoto && tC4.CRD_PRESENT_IND != "1") Warnings += "! CRD-PRESENT-IND on Token C4 should be 1." + Environment.NewLine;
                        if (!IsMoto && tC4.CRDHLDR_ACTVT_TERM_IND != "6") Warnings += "! CRDHLDR-ACTVT-TERM-IND on Token C4 should be 6." + Environment.NewLine;
                        if (!IsMoto && tC4.TERM_INPUT_CAP_IND != "6") Warnings += "! TERM-INPUT-CAP-IND on Token C4 should be 6." + Environment.NewLine;
                        if (!IsMoto && tC4.CRDHLDR_ID_METHOD != "3") Warnings += "! CRDHLDR-ID-METHOD on Token C4 should be 3." + Environment.NewLine;
                    }

                }
                catch (Exception ex)
                {
                    Warnings += "! Error on CnP Validations." + Environment.NewLine;
                    Warnings += ex.Message + Environment.NewLine;
                }
            }
            #endregion

            #region MoTo
            if (IsMoto)
            {
                try
                {
                    if (IsCP) Warnings += "! The MoTo transaction was formatted as CP." + Environment.NewLine;
                    if (IsRecurring) Warnings += "! The MoTo transaction was formatted as Recurring." + Environment.NewLine;
                    if (!string.IsNullOrWhiteSpace(PrimaryDEs[24]) && PrimaryDEs[24] != "08") Warnings += "! Data Element [25] was not '08'." + Environment.NewLine;

                    if (tC0 != null)
                    {
                        if (!string.IsNullOrWhiteSpace(tC0.CVD_FLD)) Warnings += "! CVD-FLD on Token C0 should be empty." + Environment.NewLine;
                        if (tC0.E_COM_FLG != "1") Warnings += "! E-COM-FLG on Token C0 should be 1." + Environment.NewLine;
                        if (tC0.CVD_FLD_PRESENT != "0") Warnings += "! CVD-FLD-PRESENT on Token C0 should be 0." + Environment.NewLine;
                    }

                    if (tC4 != null)
                    {
                        if (tC4.TERM_ATTEND_IND != "0") Warnings += "! TERM-ATTEND-IND on Token C4 should be 0." + Environment.NewLine;
                        if (tC4.TERM_LOC_IND != "0") Warnings += "! TERM-LOC-IND on Token C4 should be 0." + Environment.NewLine;
                        if (tC4.CRDHLDR_PRESENT_IND != "2") Warnings += "! CRDHLDR-PRESENT-IND on Token C4 should be 2." + Environment.NewLine;
                        if (tC4.CRD_PRESENT_IND != "1") Warnings += "! CRD-PRESENT-IND on Token C4 should be 1." + Environment.NewLine;
                        if (tC4.CRDHLDR_ACTVT_TERM_IND != "0") Warnings += "! CRDHLDR-ACTVT-TERM-IND on Token C4 should be 0." + Environment.NewLine;
                        if (tC4.TERM_INPUT_CAP_IND != "6") Warnings += "! TERM-INPUT-CAP-IND on Token C4 should be 6." + Environment.NewLine;
                        if (tC4.CRDHLDR_ID_METHOD != "4") Warnings += "! CRDHLDR-ID-METHOD on Token C4 should be 4." + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    Warnings += "! Error on MoTo Validations." + Environment.NewLine;
                    Warnings += ex.Message + Environment.NewLine;
                }
            }
            #endregion

            #region Recurring
            if (IsRecurring)
            {
                try
                {
                    if (IsCP) Warnings += "! The Recurring transaction was formatted as CP." + Environment.NewLine;
                    if (IsMoto) Warnings += "! The Recurring transaction was formatted as MoTo." + Environment.NewLine;

                    if (tCH != null)
                    {
                        if (tCH.EMV_CAPABLE_OUTLET != "N") Warnings += "! EMV-CAPABLE-OUTLET on Token CH should be N." + Environment.NewLine;
                        if (tCH.TERM_TYP != " ") Warnings += "! TERM-TYP on Token CH should be ' '." + Environment.NewLine;
                        if (!(tCH.PMNT_INFO == "0" || tCH.PMNT_INFO == "1")) Warnings += "! PMNT-INFO on Token CH should be 0 or 1." + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    Warnings += "! Error on Recurring Validations." + Environment.NewLine;
                    Warnings += ex.Message + Environment.NewLine;
                }
            }
            #endregion

            #region Void or Reversal
            if (IsVoid || IsReversal)
            {
                try
                {
                    if (IsReversal && string.IsNullOrWhiteSpace(PrimaryDEs[14])) Warnings += "! Data Element [15] was not present in the message!" + Environment.NewLine;
                    if (string.IsNullOrWhiteSpace(PrimaryDEs[38])) Warnings += "! Data Element [39] was not present in the message!" + Environment.NewLine;
                    if (string.IsNullOrWhiteSpace(SecondaryDEs[25])) Warnings += "! Data Element [90] was not present in the message!" + Environment.NewLine;
                    if (IsReversal && string.IsNullOrWhiteSpace(SecondaryDEs[58])) Warnings += "! Data Element [123] was not present in the message!" + Environment.NewLine;
                }
                catch (Exception ex)
                {
                    Warnings += "! Error on Void Validations." + Environment.NewLine;
                    Warnings += ex.Message + Environment.NewLine;
                }
            }
            #endregion

            #endregion
        }

        public int GetDERule(int de, string msg)
        {
            if (l1.Contains(de)) return 1;
            else if (l2.Contains(de)) return 2;
            else if (l3.Contains(de)) return 3;
            else if (l4.Contains(de)) return 4;
            else if (l5.Contains(de)) return 5;
            else if (l6.Contains(de)) return 6;
            else if (l7.Contains(de)) return 7;
            else if (l8.Contains(de)) return 8;
            else if (l10.Contains(de)) return 10;
            else if (l12.Contains(de)) return 12;
            else if (l15.Contains(de)) return 15;
            else if (l16.Contains(de)) return 16;
            else if (l25.Contains(de)) return 25;
            else if (l40.Contains(de)) return 40;
            else if (l42.Contains(de)) return 42;
            else if (lvar2.Contains(de)) return int.Parse(msg.Substring(0, 2)) + 2;
            else if (lvar3.Contains(de)) return int.Parse(msg.Substring(0, 3)) + 3;

            return 0;
        }

        public BicIsoMessage Reply()
        {
            BicIsoMessage rmsg = new BicIsoMessage();
            try
            {
                rmsg.Header = "ISO026000070"; // ?? check real response for valid values
                if (MTI == "0100") rmsg.MTI = "0110";
                else if (MTI == "0200") rmsg.MTI = "0210";
                else if (MTI == "0220" || MTI == "0221") rmsg.MTI = "0230";
                else if (MTI == "0420" || MTI == "0421") rmsg.MTI = "0430";
                else if (MTI == "0800") rmsg.MTI = "0810";
                else if (MTI == "0820" || MTI == "0821") rmsg.MTI = "0830";

                // echo every field included in the original transaction
                for (int i = 0; i < 64; i++)
                {
                    rmsg.PrimaryDEs[i] = PrimaryDEs[i];
                    rmsg.SecondaryDEs[i] = SecondaryDEs[i];
                }

                // add or remove fields
                if (rmsg.MTI.StartsWith("08")) rmsg.PrimaryDEs[38] = "00";
                else
                {
                    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    var cm = ((AppSettingsSection)config.GetSection("ResponseCodes")).Settings;
                    string pan = "";
                    if (PrimaryDEs[34].Contains("D")) pan = PrimaryDEs[34].Substring(2, PrimaryDEs[34].IndexOf("D") - 2);
                    if (PrimaryDEs[34].Contains("=")) pan = PrimaryDEs[34].Substring(2, PrimaryDEs[34].IndexOf("=") - 2);
                    string rc = "00"; // if pan not configured rc -> 00
                    try
                    {
                        rc = cm[pan].Value; // get value from configuration
                    }
                    catch { }

                    rmsg.PrimaryDEs[0] = "a";
                    rmsg.PrimaryDEs[14] = string.Format("{0:MMdd}", DateTime.Now.AddDays(1));
                    rmsg.PrimaryDEs[17] = string.Empty;
                    rmsg.PrimaryDEs[37] = new Random(int.Parse(PrimaryDEs[10])).Next(100001, 999999).ToString();
                    rmsg.PrimaryDEs[38] = rc;
                    rmsg.PrimaryDEs[51] = string.Empty;
                    rmsg.SecondaryDEs[59] = string.Empty;
                    rmsg.SecondaryDEs[62] = "007       ";

                    // Tokens add or remove
                    // ...
                }

                // handle bitmaps
                BitArray p = new BitArray(64);
                BitArray s = new BitArray(64);
                for (int i = 0; i < 64; i++)
                {
                    if (!string.IsNullOrEmpty(rmsg.PrimaryDEs[i])) p[i] = true;
                    if (!string.IsNullOrEmpty(rmsg.SecondaryDEs[i])) s[i] = true;
                }
                string bmap1 = "";
                string bmap2 = "";
                for (int i = 0; i < 64; i += 4)
                {
                    int y = 0;
                    if (p[i]) y += 8;
                    if (p[i + 1]) y += 4;
                    if (p[i + 2]) y += 2;
                    if (p[i + 3]) y += 1;
                    bmap1 += y.ToString("X");
                    y = 0;
                    if (s[i]) y += 8;
                    if (s[i + 1]) y += 4;
                    if (s[i + 2]) y += 2;
                    if (s[i + 3]) y += 1;
                    bmap2 += y.ToString("X");
                }

                rmsg.PrimaryBitmap = bmap1;
                rmsg.PrimaryDEs[0] = bmap2;
            }
            catch { }

            return rmsg;
        }
    }
}
