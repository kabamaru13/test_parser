﻿namespace TestParser
{
    class C0 : Token
    {
        public string CVD_FLD { get; set; } // p1-4
        public string RESUB_STAT { get; set; } // p5
        public string RESUB_CNTR { get; set; } // p6-8
        public string TERM_POSTAL_CDE { get; set; } // p9-18
        public string E_COM_FLG { get; set; } // p19
        public string CMRCL_CRD_TYP { get; set; } // p20
        public string ADNL_DATA_IND { get; set; } // p21
        public string CVD_FLD_PRESENT { get; set; } // p22
        public string MRCH_SRVC_RQST { get; set; } // p23
        public string AUTHN_COLL_IND { get; set; } // p24
        public string FRD_PRN_FLG { get; set; } // p25
        public string CAVV_AAV_RSLT_CDE { get; set; } // p26

        public C0(string h, string n, string d) : base(h, n, d)
        {
            CVD_FLD = d.Substring(0, 4);
            RESUB_STAT = d.Substring(4, 1);
            RESUB_CNTR = d.Substring(5, 3);
            TERM_POSTAL_CDE = d.Substring(8, 10);
            E_COM_FLG = d.Substring(18, 1);
            CMRCL_CRD_TYP = d.Substring(19, 1);
            ADNL_DATA_IND = d.Substring(20, 1);
            CVD_FLD_PRESENT = d.Substring(21, 1);
            MRCH_SRVC_RQST = d.Substring(22, 1);
            AUTHN_COLL_IND = d.Substring(23, 1);
            FRD_PRN_FLG = d.Substring(24, 1);
            CAVV_AAV_RSLT_CDE = d.Substring(25, 1);
        }
    }
}
