﻿namespace TestParser
{
    class C4 : Token
    {
        public string TERM_ATTEND_IND { get; set; } // p1
        public string TERM_OPER_IND { get; set; } // p2
        public string TERM_LOC_IND { get; set; } // p3
        public string CRDHLDR_PRESENT_IND { get; set; } // p4
        public string CRD_PRESENT_IND { get; set; } // p5
        public string CRD_CAPTR_IND { get; set; } // p6
        public string TXN_STAT_IND { get; set; } // p7
        public string TXN_SEC_IND { get; set; } // p8
        public string TXN_RTN_IND { get; set; } // p9
        public string CRDHLDR_ACTVT_TERM_IND { get; set; } // p10
        public string TERM_INPUT_CAP_IND { get; set; } // p11
        public string CRDHLDR_ID_METHOD { get; set; } // p12

        public C4(string h, string n, string d) : base(h, n, d)
        {
            TERM_ATTEND_IND = d.Substring(0, 1);
            TERM_OPER_IND = d.Substring(1, 1);
            TERM_LOC_IND = d.Substring(2, 1);
            CRDHLDR_PRESENT_IND = d.Substring(3, 1);
            CRD_PRESENT_IND = d.Substring(4, 1);
            CRD_CAPTR_IND = d.Substring(5, 1);
            TXN_STAT_IND = d.Substring(6, 1);
            TXN_SEC_IND = d.Substring(7, 1);
            TXN_RTN_IND = d.Substring(8, 1);
            CRDHLDR_ACTVT_TERM_IND = d.Substring(9, 1);
            TERM_INPUT_CAP_IND = d.Substring(10, 1);
            CRDHLDR_ID_METHOD = d.Substring(11, 1);
        }
    }
}
