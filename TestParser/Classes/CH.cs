﻿namespace TestParser
{
    class CH : Token
    {
        public string RESP_SRC_RSN_CDE { get; set; } // p1
        public string CRD_VRFY_FLG2 { get; set; } // p2
        public string ONLINE_LMT { get; set; } // p3-14
        public string RETL_CLASS_CDE { get; set; } // p15-18
        public string EMV_CAPABLE_OUTLET { get; set; } // p19
        public string RECUR_PMNT_IND { get; set; } // p20
        public string NUM_INSTL { get; set; } // p21-22
        public string NUM_MM_GRATUITY { get; set; } // p23-24
        public string PMNT_PLAN { get; set; } // p25-27
        public string TERM_OUTPUT_CAP_IND { get; set; } // p28
        public string CRDHLDR_AUTHN_CAP_IND { get; set; } // p29
        public string PARTIAL_AUTH_OPT { get; set; } // p30
        public string INSTL_PLAN_TYP { get; set; } // p31-32
        public string INSTL_GRATUITY_PRD { get; set; } // p33
        public string RVSL_RSN_IND { get; set; } // p34
        public string FAILED_CVM_ALWD { get; set; } // p35
        public string DUP_CHK_REQ { get; set; } // p36
        public string AUTH_MSG_IND { get; set; } // p37
        public string TERM_TYP { get; set; } // p38
        public string PMNT_INFO { get; set; } // p39
        public string USER_FLD1 { get; set; } // p40

        public CH(string h, string n, string d) : base(h, n, d)
        {
            RESP_SRC_RSN_CDE = d.Substring(0, 1);
            CRD_VRFY_FLG2 = d.Substring(1, 1);
            ONLINE_LMT = d.Substring(2, 12);
            RETL_CLASS_CDE = d.Substring(14, 4);
            EMV_CAPABLE_OUTLET = d.Substring(18, 1);
            RECUR_PMNT_IND = d.Substring(19, 1);
            NUM_INSTL = d.Substring(20, 2);
            NUM_MM_GRATUITY = d.Substring(22, 2);
            PMNT_PLAN = d.Substring(24, 3);
            TERM_OUTPUT_CAP_IND = d.Substring(27, 1);
            CRDHLDR_AUTHN_CAP_IND = d.Substring(28, 1);
            PARTIAL_AUTH_OPT = d.Substring(29, 1);
            INSTL_PLAN_TYP = d.Substring(30, 2);
            INSTL_GRATUITY_PRD = d.Substring(32, 1);
            RVSL_RSN_IND = d.Substring(33, 1);
            FAILED_CVM_ALWD = d.Substring(34, 1);
            DUP_CHK_REQ = d.Substring(35, 1);
            AUTH_MSG_IND = d.Substring(36, 1);
            TERM_TYP = d.Substring(37, 1);
            PMNT_INFO = d.Substring(38, 1);
            USER_FLD1 = d.Substring(39, 1);
        }
    }
}
