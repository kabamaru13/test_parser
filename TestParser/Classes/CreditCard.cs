﻿namespace TestParser
{
    class CreditCard
    {
        public string Pan { get; set; }
        public string Track2 { get; set; }
        public string CVV { get; set; }
        public string CVV2 { get; set; }
        public string Pin { get; set; }
        public string Type { get; set; }
        public string ExpirationDate { get; set; }
        public string Balance { get; set; }
        public string ResponseCode { get; set; }

        public CreditCard()
        {

        }
    }
}
