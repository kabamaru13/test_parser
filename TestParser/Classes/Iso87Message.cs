﻿using System;
using System.Collections;
using System.Configuration;
using System.Linq;

namespace TestParser
{
    class Iso87Message : IsoMessage
    {
        int[] l1 = { 27, 66, 91 };
        int[] l2 = { 25, 26, 39, 67, 92 };
        int[] l3 = { 19, 20, 21, 22, 23, 24, 40, 49, 50, 51, 68, 69, 70 };
        int[] l4 = { 13, 14, 15, 16, 17, 18, 71, 72 };
        int[] l5 = { 93 };
        int[] l6 = { 3, 11, 12, 38, 73 };
        int[] l7 = { 94 };
        int[] l8 = { 8, 9, 10, 41 };
        int[] l9 = { 28, 29, 30, 31 };
        int[] l10 = { 7, 74, 75, 76, 77, 78, 79, 80, 81 };
        int[] l12 = { 4, 5, 6, 37, 82, 83, 84, 85 };
        int[] l15 = { 42 };
        int[] l16 = { 1, 52, 53, 64, 65, 86, 87, 88, 89, 96, 125, 128 };
        int[] l17 = { 97 };
        int[] l25 = { 98 };
        int[] l40 = { 43 };
        int[] l42 = { 90, 95 };
        int[] lvar2 = { 2, 32, 33, 34, 35, 44, 45, 99, 100, 101, 102, 103 };
        int[] lvar3 = { 36, 46, 47, 48, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 126, 127 };

        public Iso87Message()
        {
            PrimaryDEs = new string[64];
            SecondaryDEs = new string[64];
            Created = DateTime.Now;
            Type = 1;
        }

        public Iso87Message(string msg)
        {
            PrimaryDEs = new string[64];
            SecondaryDEs = new string[64];
            Created = DateTime.Now;
            Type = 1;

            if (string.IsNullOrWhiteSpace(msg))
            {
                Errors = "Message was empty!";
                return;
            }

            if (msg.StartsWith("ISO")) // msg contains Header
            {
                Header = msg.Substring(0, 12);
                msg = msg.Substring(12);
            }

            if (msg.Length < 20)
            {
                Errors = "Message length was smaller than 20 characters!";
                return;
            }

            MTI = msg.Substring(0, 4);
            if (int.Parse(MTI[2].ToString()) % 2 == 0) IsRequest = true;
            else IsRequest = false;
            PrimaryBitmap = msg.Substring(4, 16);

            if (msg.Length == 20)
            {
                Warnings += "! Message contained only the primary bitmap!" + Environment.NewLine;
                return;
            }
            msg = msg.Substring(20); // message left are the DEs

            BitArray bits = HandleBitmap(PrimaryBitmap);

            int i = 0;
            try
            {
                foreach (bool bit in bits)
                {
                    if (bit)
                    {
                        PrimaryDEs[i] = msg.Substring(0, GetDERule(i + 1, msg));
                        msg = msg.Substring(GetDERule(i + 1, msg));
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                Errors = "Error on data element " + (i + 1) + "." + Environment.NewLine;
                Errors += ex.Message;
                return;
            }

            try
            {
                i = 0;
                bits = HandleBitmap(PrimaryDEs[0]);
                foreach (bool bit in bits)
                {
                    if (bit)
                    {
                        SecondaryDEs[i] = msg.Substring(0, GetDERule(i + 65, msg));
                        msg = msg.Substring(GetDERule(i + 65, msg));
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                Errors = "Error on data element " + (i + 65) + "." + Environment.NewLine;
                Errors += ex.Message;
                return;
            }
        }

        public int GetDERule(int de, string msg)
        {
            if (l1.Contains(de)) return 1;
            else if (l2.Contains(de)) return 2;
            else if (l3.Contains(de)) return 3;
            else if (l4.Contains(de)) return 4;
            else if (l5.Contains(de)) return 5;
            else if (l6.Contains(de)) return 6;
            else if (l7.Contains(de)) return 7;
            else if (l8.Contains(de)) return 8;
            else if (l9.Contains(de)) return 9;
            else if (l10.Contains(de)) return 10;
            else if (l12.Contains(de)) return 12;
            else if (l15.Contains(de)) return 15;
            else if (l16.Contains(de)) return 16;
            else if (l17.Contains(de)) return 17;
            else if (l25.Contains(de)) return 25;
            else if (l40.Contains(de)) return 40;
            else if (l42.Contains(de)) return 42;
            else if (lvar2.Contains(de)) return int.Parse(msg.Substring(0, 2)) + 2;
            else if (lvar3.Contains(de)) return int.Parse(msg.Substring(0, 3)) + 3;

            return 0;
        }
        
        public new string PrintIso()
        {
            string disp = base.PrintIso();

            return disp;
        }

        public Iso87Message Reply()
        {
            Iso87Message rmsg = new Iso87Message();
            
            return rmsg;
        }
    }
}
