﻿using System;
using System.Collections;

namespace TestParser
{
    class IsoMessage
    {
        public string Header { get; set; }
        public string MTI { get; set; }
        public string PrimaryBitmap { get; set; }
        public string[] PrimaryDEs { get; set; }
        public string[] SecondaryDEs { get; set; }
        public string Errors { get; set; }
        public string Warnings { get; set; }
        public DateTime Created { get; set; }
        public int Type { get; set; } // 0: No type, 1: Iso8583 87', 2: Iso8583 93', 3: BicIso - Base24
        public bool IsRequest { get; set; }
        public IsoMessage LinkedMessage { get; set; }

        public string PrintIso()
        {
            string disp = "";
            if (!string.IsNullOrWhiteSpace(Header)) disp += "- Header: " + Header + Environment.NewLine;
            disp += "- MTI: " + MTI + Environment.NewLine;
            disp += "- Primary Bitmap: " + PrimaryBitmap + Environment.NewLine;
            int i = 1;

            foreach (var de in PrimaryDEs)
            {
                if (!string.IsNullOrWhiteSpace(de)) disp += "- DE " + i + ": " + de + Environment.NewLine;
                i++;
            }

            i = 65;
            foreach (var de in SecondaryDEs)
            {
                if (!string.IsNullOrWhiteSpace(de)) disp += "- DE " + i + ": " + de + Environment.NewLine;
                i++;
            }
            disp += Environment.NewLine;

            return disp;
        }

        public string PrintLog()
        {
            if (!string.IsNullOrWhiteSpace(Errors)) return "Errors:" + Environment.NewLine + Errors;
            else if (!string.IsNullOrWhiteSpace(Warnings)) return "Warnings:" + Environment.NewLine + Warnings;
            else return "No Warnings";
        }

        public BitArray HandleBitmap(string bitmap)
        {
            BitArray bits = new BitArray(64);

            string binaryVals = Convert.ToString(Convert.ToInt64(bitmap, 16), 2).PadLeft(bitmap.Length * 4, '0');
            int i = 0;
            foreach (var b in binaryVals)
            {
                if (b == '1') bits[i] = true;
                else bits[i] = false;
                i++;
            }

            return bits;
        }

        public override string ToString()
        {
            return MTI + " - " + string.Format("{0: dd/MM/yyyy hh:mm:ss}", Created);
        }

        public string ToReplyString()
        {
            string ret = Header + MTI + PrimaryBitmap;
            foreach (string de in PrimaryDEs) ret += de;
            foreach (string de in SecondaryDEs) ret += de;
            return ret;
        }
    }
}
