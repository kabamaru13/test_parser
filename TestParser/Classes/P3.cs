﻿namespace TestParser
{
    class P3 : Token
    {
        public string BYPASS_RULES_FLG { get; set; } // p1
        public string FILLER1 { get; set; } // p2
        public string BANK_ID { get; set; } // p3-5
        public string FILLER2 { get; set; } // p6
        public string XLS_ID { get; set; } // p7-21
        public string FILLER3 { get; set; } // p22
        public string DISTANCE_IND { get; set; } // p23-30
        public string FDH_LOYALTY_AMT { get; set; } // p31-42
        public string LOYALTY_AMT_ADJ_STA { get; set; } // p43
        public string ORIG_MSG_TYP { get; set; } // p44-47
        public string USER_FLD_CUST { get; set; } // p48-80

        public P3(string h, string n, string d) : base(h, n, d)
        {
            BYPASS_RULES_FLG = d.Substring(0, 1);
            FILLER1 = d.Substring(1, 1);
            BANK_ID = d.Substring(2, 3);
            FILLER2 = d.Substring(5, 1);
            XLS_ID = d.Substring(6, 15);
            FILLER3 = d.Substring(21, 1);
            DISTANCE_IND = d.Substring(22, 8);
            FDH_LOYALTY_AMT = d.Substring(30, 12);
            LOYALTY_AMT_ADJ_STA = d.Substring(42, 1);
            ORIG_MSG_TYP = d.Substring(43, 4);
            USER_FLD_CUST = d.Substring(47, 33);
        }
    }
}
