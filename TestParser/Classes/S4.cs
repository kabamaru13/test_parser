﻿namespace TestParser
{
    class S4 : Token
    {
        public string DATASET_ID { get; set; } // p1-2
        public string LGTH { get; set; } // p3-6
        public string SUPPL_DATA { get; set; } // p7-166

        public S4(string h, string n, string d) : base(h, n, d)
        {
            DATASET_ID = d.Substring(0, 2);
            LGTH = d.Substring(2, 4);
            SUPPL_DATA = d.Substring(6, 160);
        }
    }
}
