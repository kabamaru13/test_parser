﻿namespace TestParser
{
    class Token
    {
        public string Header { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }

        public Token(string h, string n, string d)
        {
            Header = h;
            Name = n;
            Data = d;
        }
    }
}
