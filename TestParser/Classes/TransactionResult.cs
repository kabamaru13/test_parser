﻿using System;

namespace TestParser
{
    public class TransactionResult
    {
        public string StatusId { get; set; }
        public Guid TransactionId { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorText { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
