﻿namespace TestParser
{
    class _04 : Token
    {
        public string ERR_FLG { get; set; } // p1
        public string RTE_GRP { get; set; } // p2-12
        public string CRD_VRFY_FLG { get; set; } // p13
        public string CITY_EXT { get; set; } // p14-18
        public string COMPLETE_TRACK2_DATA { get; set; } // p19
        public string UAF_FLG { get; set; } // p20

        public _04(string h, string n, string d) : base(h, n, d)
        {
            ERR_FLG = d.Substring(0, 1);
            RTE_GRP = d.Substring(1, 11);
            CRD_VRFY_FLG = d.Substring(12, 1);
            CITY_EXT = d.Substring(13, 5);
            COMPLETE_TRACK2_DATA = d.Substring(18, 1);
            UAF_FLG = d.Substring(19, 1);
        }
    }
}
