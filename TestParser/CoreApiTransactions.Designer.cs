﻿namespace TestParser
{
    partial class CoreApiTransactions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BaseURL = new System.Windows.Forms.TextBox();
            this.OrderURL = new System.Windows.Forms.TextBox();
            this.TransactionURL = new System.Windows.Forms.TextBox();
            this.Username = new System.Windows.Forms.TextBox();
            this.Password = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Amount = new System.Windows.Forms.TextBox();
            this.CardNumber = new System.Windows.Forms.TextBox();
            this.ExpDate = new System.Windows.Forms.TextBox();
            this.CVV = new System.Windows.Forms.TextBox();
            this.TransactionId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ResBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.CAVV = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "BaseURL:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "OrderURL:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "TransactionURL:";
            // 
            // BaseURL
            // 
            this.BaseURL.Location = new System.Drawing.Point(104, 10);
            this.BaseURL.Name = "BaseURL";
            this.BaseURL.Size = new System.Drawing.Size(235, 20);
            this.BaseURL.TabIndex = 3;
            // 
            // OrderURL
            // 
            this.OrderURL.Location = new System.Drawing.Point(104, 37);
            this.OrderURL.Name = "OrderURL";
            this.OrderURL.Size = new System.Drawing.Size(235, 20);
            this.OrderURL.TabIndex = 4;
            // 
            // TransactionURL
            // 
            this.TransactionURL.Location = new System.Drawing.Point(104, 64);
            this.TransactionURL.Name = "TransactionURL";
            this.TransactionURL.Size = new System.Drawing.Size(235, 20);
            this.TransactionURL.TabIndex = 5;
            // 
            // Username
            // 
            this.Username.Location = new System.Drawing.Point(104, 91);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(235, 20);
            this.Username.TabIndex = 6;
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(104, 118);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(235, 20);
            this.Password.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Username:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Password:";
            // 
            // Amount
            // 
            this.Amount.Location = new System.Drawing.Point(475, 10);
            this.Amount.Name = "Amount";
            this.Amount.Size = new System.Drawing.Size(235, 20);
            this.Amount.TabIndex = 10;
            // 
            // CardNumber
            // 
            this.CardNumber.Location = new System.Drawing.Point(475, 37);
            this.CardNumber.Name = "CardNumber";
            this.CardNumber.Size = new System.Drawing.Size(235, 20);
            this.CardNumber.TabIndex = 11;
            // 
            // ExpDate
            // 
            this.ExpDate.Location = new System.Drawing.Point(475, 64);
            this.ExpDate.Name = "ExpDate";
            this.ExpDate.Size = new System.Drawing.Size(235, 20);
            this.ExpDate.TabIndex = 12;
            // 
            // CVV
            // 
            this.CVV.Location = new System.Drawing.Point(475, 91);
            this.CVV.Name = "CVV";
            this.CVV.Size = new System.Drawing.Size(235, 20);
            this.CVV.TabIndex = 13;
            // 
            // TransactionId
            // 
            this.TransactionId.Location = new System.Drawing.Point(475, 144);
            this.TransactionId.Name = "TransactionId";
            this.TransactionId.Size = new System.Drawing.Size(235, 20);
            this.TransactionId.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(394, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Amount:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(394, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Card Number:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(394, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Exp. Date:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(394, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "CVV2:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(394, 147);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "TransactionId:";
            // 
            // TType
            // 
            this.TType.FormattingEnabled = true;
            this.TType.Items.AddRange(new object[] {
            "Charge Card (E-Commerce)",
            "Charge Card (Recurring)",
            "Charge Card (MoTo)",
            "PreAuth Card (E-Commerce)",
            "PreAuth Card (MoTo)",
            "Capture Transaction",
            "Recurring Transaction",
            "Void/Refund Transaction",
            "OCT",
            "Charge 3D Secure"});
            this.TType.Location = new System.Drawing.Point(104, 144);
            this.TType.Name = "TType";
            this.TType.Size = new System.Drawing.Size(235, 21);
            this.TType.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 147);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Transaction:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(231, 171);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(235, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "Execute Transaction";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ResBox
            // 
            this.ResBox.Enabled = false;
            this.ResBox.Location = new System.Drawing.Point(16, 200);
            this.ResBox.Multiline = true;
            this.ResBox.Name = "ResBox";
            this.ResBox.Size = new System.Drawing.Size(694, 52);
            this.ResBox.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(394, 121);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Cavv:";
            // 
            // CAVV
            // 
            this.CAVV.Location = new System.Drawing.Point(475, 118);
            this.CAVV.Name = "CAVV";
            this.CAVV.Size = new System.Drawing.Size(235, 20);
            this.CAVV.TabIndex = 25;
            // 
            // CoreApiTransactions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 264);
            this.Controls.Add(this.CAVV);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.ResBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TType);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TransactionId);
            this.Controls.Add(this.CVV);
            this.Controls.Add(this.ExpDate);
            this.Controls.Add(this.CardNumber);
            this.Controls.Add(this.Amount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.Username);
            this.Controls.Add(this.TransactionURL);
            this.Controls.Add(this.OrderURL);
            this.Controls.Add(this.BaseURL);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CoreApiTransactions";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CoreApiTransactions";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox BaseURL;
        private System.Windows.Forms.TextBox OrderURL;
        private System.Windows.Forms.TextBox TransactionURL;
        private System.Windows.Forms.TextBox Username;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Amount;
        private System.Windows.Forms.TextBox CardNumber;
        private System.Windows.Forms.TextBox ExpDate;
        private System.Windows.Forms.TextBox CVV;
        private System.Windows.Forms.TextBox TransactionId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox TType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox ResBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox CAVV;
    }
}