﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RestSharp;
using RestSharp.Authenticators;

namespace TestParser
{
    public partial class CoreApiTransactions : Form
    {
        public CoreApiTransactions()
        {
            InitializeComponent();
            BaseURL.Text = "http://dev.vivapayments.com";
            OrderURL.Text = "/api/orders";
            TransactionURL.Text = "/api/transactions";
            Username.Text = "51638f3b-4d32-4837-a47d-b6917953a183";
            Password.Text = "51638F3B-4D32-4837-A47D-B6917953A183";
        }

        private long createOrder(ApiData dato)
        {
            var client = new RestClient(dato.BaseUrl);
            client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
            var request = new RestRequest(dato.OrderUrl, Method.POST);
            request.Timeout = 5000;
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new { Amount = dato.Amount, SourceCode = "Default" });

            var res = client.Execute<OrderResult>(request);

            if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.OrderCode;
            else return 0;
        }

        private long createRecurringOrder(ApiData dato)
        {
            var client = new RestClient(dato.BaseUrl);
            client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
            var request = new RestRequest(dato.OrderUrl, Method.POST);
            request.Timeout = 5000;
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new { Amount = dato.Amount, SourceCode = "Default", AllowRecurring = true });

            var res = client.Execute<OrderResult>(request);

            if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.OrderCode;
            else return 0;
        }

        private long createSecOrder(ApiData dato)
        {
            var client = new RestClient(dato.BaseUrl);
            client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
            var request = new RestRequest(dato.OrderUrl, Method.POST);
            request.Timeout = 5000;
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new { Amount = dato.Amount, SourceCode = "Default", MerchantTrns = "01234567FEEDCAFE650432347202239605319710", CustomerTrns = string.Format("0000010{0}799300000000000000000000000000", dato.Cavv) });

            var res = client.Execute<OrderResult>(request);

            if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.OrderCode;
            else return 0;
        }

        private string chargeCard(ApiData dato)
        {
            try
            {
                dato.OrderCode = createOrder(dato);
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    OrderCode = dato.OrderCode,
                    creditcard = new { cardholdername = "QA QA", number = dato.CardNumber, expirationDate = dato.ExpDate, cvc = dato.Cvv },
                    paymentmethodid = 0
                };
                request.AddBody(json);

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch(Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string chargeCardRec(ApiData dato)
        {
            try
            {
                dato.OrderCode = createRecurringOrder(dato);
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    OrderCode = dato.OrderCode,
                    creditcard = new { cardholdername = "QA QA", number = dato.CardNumber, expirationDate = dato.ExpDate, cvc = dato.Cvv },
                    AllowsRecurring = true,
                    paymentmethodid = 0
                };
                request.AddBody(json);

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch(Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string preAuthCard(ApiData dato)
        {
            try
            {
                dato.OrderCode = createOrder(dato);
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    OrderCode = dato.OrderCode,
                    creditcard = new { cardholdername = "QA QA", number = dato.CardNumber, expirationDate = dato.ExpDate, cvc = dato.Cvv },
                    paymentmethodid = 1
                };
                request.AddBody(json);

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch(Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string motoCharge(ApiData dato)
        {
            try
            {
                dato.OrderCode = createOrder(dato);
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    OrderCode = dato.OrderCode,
                    creditcard = new { cardholdername = "QA QA", number = dato.CardNumber, expirationDate = dato.ExpDate, cvc = dato.Cvv },
                    paymentmethodid = 0
                };
                request.AddBody(json);
                request.AddHeader("ChannelId", "2b259f39-606a-4ce0-883f-b5a59cb9e348");

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch (Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string motoPreauth(ApiData dato)
        {
            try
            {
                dato.OrderCode = createOrder(dato);
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    OrderCode = dato.OrderCode,
                    creditcard = new { cardholdername = "QA QA", number = dato.CardNumber, expirationDate = dato.ExpDate, cvc = dato.Cvv },
                    paymentmethodid = 1
                };
                request.AddBody(json);
                request.AddHeader("ChannelId", "2b259f39-606a-4ce0-883f-b5a59cb9e348");

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch (Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string cpCharge(ApiData dato)
        {
            try
            {
                dato.OrderCode = createOrder(dato);
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    OrderCode = dato.OrderCode,
                    creditcard = new { emv = dato.CardNumber },
                    paymentmethodid = 0
                };
                request.AddBody(json);

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch (Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string cpPreauth(ApiData dato)
        {
            try
            {
                dato.OrderCode = createOrder(dato);
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    OrderCode = dato.OrderCode,
                    creditcard = new { emv = dato.CardNumber },
                    paymentmethodid = 1
                };
                request.AddBody(json);

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch (Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string captureTransaction(ApiData dato)
        {
            try
            {
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl + "/" + dato.TransactionId, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    Installments = 0,
                    Amount = dato.Amount
                };
                request.AddBody(json);

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch (Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string recurringTransaction(ApiData dato)
        {
            try
            {
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl + "/" + dato.TransactionId, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    Installments = 0,
                    Amount = dato.Amount
                };
                request.AddBody(json);

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch (Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string voidTransaction(ApiData dato)
        {
            try
            {
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                string restUrl = string.Format("{0}/{1}?amount={2}", dato.TransactionUrl, dato.TransactionId, dato.Amount);
                var request = new RestRequest(restUrl, Method.DELETE);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch (Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string octTransaction(ApiData dato)
        {
            try
            {
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                string restUrl = string.Format("{0}/{1}?amount={2}&ServiceId=6", dato.TransactionUrl, dato.TransactionId, dato.Amount);
                var request = new RestRequest(restUrl, Method.DELETE);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch (Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private string charge3dSec(ApiData dato)
        {
            try
            {
                dato.OrderCode = createSecOrder(dato);
                var client = new RestClient(dato.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(dato.Username, dato.Password);
                var request = new RestRequest(dato.TransactionUrl, Method.POST);
                request.Timeout = 15000;
                request.RequestFormat = DataFormat.Json;
                var json = new
                {
                    OrderCode = dato.OrderCode,
                    creditcard = new { cardholdername = "QA QA", number = dato.CardNumber, expirationDate = dato.ExpDate, cvc = dato.Cvv },
                    paymentmethodid = 0
                };
                request.AddBody(json);

                var res = client.Execute<TransactionResult>(request);

                ResBox.Text = "Error Code: " + res.Data.ErrorCode;
                dato.ErrorCode = res.Data.ErrorCode;
                dato.ErrorText = res.Data.ErrorText;
                if (res.Data != null && res.Data.ErrorCode == 0) return res.Data.TransactionId.ToString();
                else return "";
            }
            catch (Exception ex)
            {
                ResBox.Text = ex.Message;
                return "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ApiData dato = new ApiData();
            ResBox.Text = "...";

            try
            {
                if (!string.IsNullOrWhiteSpace(BaseURL.Text)) dato.BaseUrl = BaseURL.Text;
                if (!string.IsNullOrWhiteSpace(OrderURL.Text)) dato.OrderUrl = OrderURL.Text;
                if (!string.IsNullOrWhiteSpace(TransactionURL.Text)) dato.TransactionUrl = TransactionURL.Text;
                if (!string.IsNullOrWhiteSpace(Username.Text)) dato.Username = Username.Text;
                if (!string.IsNullOrWhiteSpace(Password.Text)) dato.Password = Password.Text;
                if (!string.IsNullOrWhiteSpace(Amount.Text)) dato.Amount = long.Parse(Amount.Text);
                if (!string.IsNullOrWhiteSpace(CardNumber.Text)) dato.CardNumber = CardNumber.Text;
                if (!string.IsNullOrWhiteSpace(ExpDate.Text)) dato.ExpDate = ExpDate.Text;
                if (!string.IsNullOrWhiteSpace(CVV.Text)) dato.Cvv = CVV.Text;
                if (!string.IsNullOrWhiteSpace(CAVV.Text)) dato.Cavv = CAVV.Text;
                if (!string.IsNullOrWhiteSpace(TransactionId.Text)) dato.TransactionId = TransactionId.Text;

                int i = TType.SelectedIndex;
                if (i == 0) TransactionId.Text = chargeCard(dato);
                if (i == 1) TransactionId.Text = chargeCardRec(dato);
                if (i == 2) TransactionId.Text = motoCharge(dato);
                if (i == 3) TransactionId.Text = preAuthCard(dato);
                if (i == 4) TransactionId.Text = motoPreauth(dato);
                if (i == 5) TransactionId.Text = captureTransaction(dato);
                if (i == 6) TransactionId.Text = recurringTransaction(dato);
                if (i == 7) TransactionId.Text = voidTransaction(dato);
                if (i == 8) TransactionId.Text = octTransaction(dato);
                if (i == 9) TransactionId.Text = charge3dSec(dato);
            }
            catch(Exception ex)
            {
                ResBox.Text = ex.Message;
            }
        }
    }
}