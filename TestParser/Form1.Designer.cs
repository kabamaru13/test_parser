﻿namespace TestParser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MessageBox = new System.Windows.Forms.TextBox();
            this.ParseB = new System.Windows.Forms.Button();
            this.Display = new System.Windows.Forms.TextBox();
            this.RDisplay = new System.Windows.Forms.TextBox();
            this.ReplyBox = new System.Windows.Forms.TextBox();
            this.MessagesList = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acquiringModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iso85831987ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iso85831993ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bicIsoBase24ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issuingModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iso85831987ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.iso85831993ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bicIsoBase24ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.parsingModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iso85831987ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.iso85831993ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.bicIsoBase24ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.networkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureCommToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creditCardResponseCodesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.apiTransactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.socketClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MessageBox
            // 
            this.MessageBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MessageBox.Location = new System.Drawing.Point(13, 25);
            this.MessageBox.Multiline = true;
            this.MessageBox.Name = "MessageBox";
            this.MessageBox.Size = new System.Drawing.Size(1573, 90);
            this.MessageBox.TabIndex = 1;
            // 
            // ParseB
            // 
            this.ParseB.Location = new System.Drawing.Point(13, 121);
            this.ParseB.Name = "ParseB";
            this.ParseB.Size = new System.Drawing.Size(75, 23);
            this.ParseB.TabIndex = 3;
            this.ParseB.Text = "Parse";
            this.ParseB.UseVisualStyleBackColor = true;
            this.ParseB.Click += new System.EventHandler(this.ParseB_Click);
            // 
            // Display
            // 
            this.Display.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Display.Location = new System.Drawing.Point(196, 150);
            this.Display.Multiline = true;
            this.Display.Name = "Display";
            this.Display.Size = new System.Drawing.Size(702, 492);
            this.Display.TabIndex = 4;
            // 
            // RDisplay
            // 
            this.RDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RDisplay.Location = new System.Drawing.Point(904, 150);
            this.RDisplay.Multiline = true;
            this.RDisplay.Name = "RDisplay";
            this.RDisplay.Size = new System.Drawing.Size(682, 492);
            this.RDisplay.TabIndex = 5;
            // 
            // ReplyBox
            // 
            this.ReplyBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReplyBox.Location = new System.Drawing.Point(13, 648);
            this.ReplyBox.Multiline = true;
            this.ReplyBox.Name = "ReplyBox";
            this.ReplyBox.Size = new System.Drawing.Size(1573, 81);
            this.ReplyBox.TabIndex = 6;
            // 
            // MessagesList
            // 
            this.MessagesList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.MessagesList.FormattingEnabled = true;
            this.MessagesList.Location = new System.Drawing.Point(13, 150);
            this.MessagesList.Name = "MessagesList";
            this.MessagesList.Size = new System.Drawing.Size(177, 485);
            this.MessagesList.TabIndex = 8;
            this.MessagesList.SelectedIndexChanged += new System.EventHandler(this.MessagesList_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.networkToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1598, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acquiringModeToolStripMenuItem,
            this.issuingModeToolStripMenuItem,
            this.parsingModeToolStripMenuItem,
            this.resetToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.fileToolStripMenuItem.Text = "Actions";
            // 
            // acquiringModeToolStripMenuItem
            // 
            this.acquiringModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iso85831987ToolStripMenuItem,
            this.iso85831993ToolStripMenuItem,
            this.bicIsoBase24ToolStripMenuItem});
            this.acquiringModeToolStripMenuItem.Name = "acquiringModeToolStripMenuItem";
            this.acquiringModeToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.acquiringModeToolStripMenuItem.Text = "Acquiring Mode";
            // 
            // iso85831987ToolStripMenuItem
            // 
            this.iso85831987ToolStripMenuItem.Name = "iso85831987ToolStripMenuItem";
            this.iso85831987ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.iso85831987ToolStripMenuItem.Text = "Iso8583-1987";
            this.iso85831987ToolStripMenuItem.Click += new System.EventHandler(this.iso85831987ToolStripMenuItem_Click);
            // 
            // iso85831993ToolStripMenuItem
            // 
            this.iso85831993ToolStripMenuItem.Name = "iso85831993ToolStripMenuItem";
            this.iso85831993ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.iso85831993ToolStripMenuItem.Text = "Iso8583-1993";
            this.iso85831993ToolStripMenuItem.Click += new System.EventHandler(this.iso85831993ToolStripMenuItem_Click);
            // 
            // bicIsoBase24ToolStripMenuItem
            // 
            this.bicIsoBase24ToolStripMenuItem.Name = "bicIsoBase24ToolStripMenuItem";
            this.bicIsoBase24ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.bicIsoBase24ToolStripMenuItem.Text = "BicIso-Base24";
            this.bicIsoBase24ToolStripMenuItem.Click += new System.EventHandler(this.bicIsoBase24ToolStripMenuItem_Click);
            // 
            // issuingModeToolStripMenuItem
            // 
            this.issuingModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iso85831987ToolStripMenuItem1,
            this.iso85831993ToolStripMenuItem1,
            this.bicIsoBase24ToolStripMenuItem1});
            this.issuingModeToolStripMenuItem.Name = "issuingModeToolStripMenuItem";
            this.issuingModeToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.issuingModeToolStripMenuItem.Text = "Issuing Mode";
            // 
            // iso85831987ToolStripMenuItem1
            // 
            this.iso85831987ToolStripMenuItem1.Name = "iso85831987ToolStripMenuItem1";
            this.iso85831987ToolStripMenuItem1.Size = new System.Drawing.Size(146, 22);
            this.iso85831987ToolStripMenuItem1.Text = "Iso8583-1987";
            this.iso85831987ToolStripMenuItem1.Click += new System.EventHandler(this.iso85831987ToolStripMenuItem1_Click);
            // 
            // iso85831993ToolStripMenuItem1
            // 
            this.iso85831993ToolStripMenuItem1.Name = "iso85831993ToolStripMenuItem1";
            this.iso85831993ToolStripMenuItem1.Size = new System.Drawing.Size(146, 22);
            this.iso85831993ToolStripMenuItem1.Text = "Iso8583-1993";
            this.iso85831993ToolStripMenuItem1.Click += new System.EventHandler(this.iso85831993ToolStripMenuItem1_Click);
            // 
            // bicIsoBase24ToolStripMenuItem1
            // 
            this.bicIsoBase24ToolStripMenuItem1.Name = "bicIsoBase24ToolStripMenuItem1";
            this.bicIsoBase24ToolStripMenuItem1.Size = new System.Drawing.Size(146, 22);
            this.bicIsoBase24ToolStripMenuItem1.Text = "BicIso-Base24";
            this.bicIsoBase24ToolStripMenuItem1.Click += new System.EventHandler(this.bicIsoBase24ToolStripMenuItem1_Click);
            // 
            // parsingModeToolStripMenuItem
            // 
            this.parsingModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iso85831987ToolStripMenuItem2,
            this.iso85831993ToolStripMenuItem2,
            this.bicIsoBase24ToolStripMenuItem2});
            this.parsingModeToolStripMenuItem.Name = "parsingModeToolStripMenuItem";
            this.parsingModeToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.parsingModeToolStripMenuItem.Text = "Parsing Mode";
            // 
            // iso85831987ToolStripMenuItem2
            // 
            this.iso85831987ToolStripMenuItem2.Name = "iso85831987ToolStripMenuItem2";
            this.iso85831987ToolStripMenuItem2.Size = new System.Drawing.Size(146, 22);
            this.iso85831987ToolStripMenuItem2.Text = "Iso8583-1987";
            this.iso85831987ToolStripMenuItem2.Click += new System.EventHandler(this.iso85831987ToolStripMenuItem2_Click);
            // 
            // iso85831993ToolStripMenuItem2
            // 
            this.iso85831993ToolStripMenuItem2.Name = "iso85831993ToolStripMenuItem2";
            this.iso85831993ToolStripMenuItem2.Size = new System.Drawing.Size(146, 22);
            this.iso85831993ToolStripMenuItem2.Text = "Iso8583-1993";
            this.iso85831993ToolStripMenuItem2.Click += new System.EventHandler(this.iso85831993ToolStripMenuItem2_Click);
            // 
            // bicIsoBase24ToolStripMenuItem2
            // 
            this.bicIsoBase24ToolStripMenuItem2.Name = "bicIsoBase24ToolStripMenuItem2";
            this.bicIsoBase24ToolStripMenuItem2.Size = new System.Drawing.Size(146, 22);
            this.bicIsoBase24ToolStripMenuItem2.Text = "BicIso-Base24";
            this.bicIsoBase24ToolStripMenuItem2.Click += new System.EventHandler(this.bicIsoBase24ToolStripMenuItem2_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // networkToolStripMenuItem
            // 
            this.networkToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureCommToolStripMenuItem});
            this.networkToolStripMenuItem.Name = "networkToolStripMenuItem";
            this.networkToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.networkToolStripMenuItem.Text = "Network";
            // 
            // configureCommToolStripMenuItem
            // 
            this.configureCommToolStripMenuItem.Name = "configureCommToolStripMenuItem";
            this.configureCommToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.configureCommToolStripMenuItem.Text = "Configure Comm";
            this.configureCommToolStripMenuItem.Click += new System.EventHandler(this.configureCommToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.creditCardResponseCodesToolStripMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.configurationToolStripMenuItem.Text = "Configuration";
            // 
            // creditCardResponseCodesToolStripMenuItem
            // 
            this.creditCardResponseCodesToolStripMenuItem.Name = "creditCardResponseCodesToolStripMenuItem";
            this.creditCardResponseCodesToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.creditCardResponseCodesToolStripMenuItem.Text = "Credit Card - Response Codes";
            this.creditCardResponseCodesToolStripMenuItem.Click += new System.EventHandler(this.creditCardResponseCodesToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.apiTransactionsToolStripMenuItem,
            this.socketClientToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // apiTransactionsToolStripMenuItem
            // 
            this.apiTransactionsToolStripMenuItem.Name = "apiTransactionsToolStripMenuItem";
            this.apiTransactionsToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.apiTransactionsToolStripMenuItem.Text = "Api Transactions";
            this.apiTransactionsToolStripMenuItem.Click += new System.EventHandler(this.apiTransactionsToolStripMenuItem_Click);
            // 
            // socketClientToolStripMenuItem
            // 
            this.socketClientToolStripMenuItem.Name = "socketClientToolStripMenuItem";
            this.socketClientToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.socketClientToolStripMenuItem.Text = "Socket Client";
            this.socketClientToolStripMenuItem.Click += new System.EventHandler(this.socketClientToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1598, 741);
            this.Controls.Add(this.MessagesList);
            this.Controls.Add(this.ReplyBox);
            this.Controls.Add(this.RDisplay);
            this.Controls.Add(this.Display);
            this.Controls.Add(this.ParseB);
            this.Controls.Add(this.MessageBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Parsing Mode - BicIso-Base24";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox MessageBox;
        private System.Windows.Forms.Button ParseB;
        private System.Windows.Forms.TextBox Display;
        private System.Windows.Forms.TextBox RDisplay;
        private System.Windows.Forms.TextBox ReplyBox;
        private System.Windows.Forms.ListBox MessagesList;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acquiringModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem issuingModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem networkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureCommToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creditCardResponseCodesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iso85831987ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iso85831993ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bicIsoBase24ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iso85831987ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem iso85831993ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bicIsoBase24ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem parsingModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iso85831987ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem iso85831993ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem bicIsoBase24ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem apiTransactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem socketClientToolStripMenuItem;
    }
}

