﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Text;
using System.Threading;

namespace TestParser
{
    public partial class Form1 : Form
    {
        List<IsoMessage> AllMessages;
        // 1: A87, 2: A93, 3: AB, 4: I87, 5: I93, 6: IB, 7: P87, 8: P93, 9: PB
        int mode = 9;
        SocketPermission permission;
        Socket sListener;
        IPEndPoint ipEndPoint;
        Socket handler;

        public Form1()
        {
            InitializeComponent();
            AllMessages = new List<IsoMessage>();
        }

        private void ParseB_Click(object sender, EventArgs e)
        {
            string msg = MessageBox.Text;
            msg = msg.Trim('"');
            
            if (mode % 3 == 1) // Iso8583 - 1987
            {
                Iso87Message Imsg = new Iso87Message(msg);

                DisplayMessage(Imsg);
                AllMessages.Add(Imsg);
            }
            else if (mode % 3 == 2) Display.Text = "Not Implemented yet"; // Iso8583 - 1993
            else if (mode % 3 == 0) // BicIso - Base24
            {
                BicIsoMessage Imsg = new BicIsoMessage(msg);

                DisplayMessage(Imsg);
                AllMessages.Add(Imsg);
            }
            UpdateMessages();
        }

        private void UpdateMessages()
        {
            MessagesList.Items.Clear();
            foreach (var msg in AllMessages)
            {
                MessagesList.Items.Add(msg.ToString());
            }
        }

        private void DisplayMessage(IsoMessage msg)
        {
            Display.Text = "";
            RDisplay.Text = "";
            ReplyBox.Text = "";

            if (msg.Type == 0) return; // IsoMessage Default
            else if (msg.Type == 1) // Iso8583 - 1987 Message
            {
                if (msg.IsRequest)
                {
                    Display.Text += ((Iso87Message)msg).PrintIso();
                    Display.Text += ((Iso87Message)msg).PrintLog();
                    if (msg.LinkedMessage != null)
                    {
                        RDisplay.Text += ((Iso87Message)msg.LinkedMessage).PrintIso();
                        RDisplay.Text += ((Iso87Message)msg.LinkedMessage).PrintLog();
                    }
                }
                else
                {
                    RDisplay.Text += ((Iso87Message)msg).PrintIso();
                    RDisplay.Text += ((Iso87Message)msg).PrintLog();
                    if (msg.LinkedMessage != null)
                    {
                        Display.Text += ((Iso87Message)msg.LinkedMessage).PrintIso();
                        Display.Text += ((Iso87Message)msg.LinkedMessage).PrintLog();
                    }
                }
            }
            else if (msg.Type == 2) return; // Iso8583 - 1993 Message
            else if (msg.Type == 3) // BicIso - Base24 Message
            {
                if (msg.IsRequest)
                {
                    Display.Text += ((BicIsoMessage)msg).PrintIso();
                    Display.Text += ((BicIsoMessage)msg).PrintLog();
                    if (msg.LinkedMessage != null)
                    {
                        RDisplay.Text += ((BicIsoMessage)msg.LinkedMessage).PrintIso();
                        RDisplay.Text += ((BicIsoMessage)msg.LinkedMessage).PrintLog();
                    }
                }
                else
                {
                    RDisplay.Text += ((BicIsoMessage)msg).PrintIso();
                    RDisplay.Text += ((BicIsoMessage)msg).PrintLog();
                    if (msg.LinkedMessage != null)
                    {
                        Display.Text += ((BicIsoMessage)msg.LinkedMessage).PrintIso();
                        Display.Text += ((BicIsoMessage)msg.LinkedMessage).PrintLog();
                    }
                }
            }
        }

        private void Reset()
        {
            AllMessages.Clear();
            UpdateMessages();
            MessageBox.Text = "";
            Display.Text = "";
            RDisplay.Text = "";
            ReplyBox.Text = "";
        }

        private void MessagesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = MessagesList.SelectedIndex;
            if (i < 0) return;
            
            DisplayMessage(AllMessages[i]);
        }

        private void other_Closed(object sender, FormClosedEventArgs e)
        {
            this.Enabled = true;
        }

        #region Menu Modes
        private void creditCardResponseCodesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            CardResponseConfig crc = new TestParser.CardResponseConfig();
            crc.FormClosed += other_Closed;
            crc.Show();
        }

        private void iso85831987ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mode != 1) Reset();
            mode = 1;
            ParseB.Enabled = false;
            this.Text = "Acquiring Mode - Iso8583-1987";
            StopServer();
        }

        private void iso85831993ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mode != 2) Reset();
            mode = 2;
            ParseB.Enabled = false;
            this.Text = "Acquiring Mode - Iso8583-1993";
            StopServer();
        }

        private void bicIsoBase24ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mode != 3) Reset();
            mode = 3;
            ParseB.Enabled = false;
            this.Text = "Acquiring Mode - BicIso-Base24";
            StopServer();
        }

        private void iso85831987ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (mode != 4) Reset();
            mode = 4;
            ParseB.Enabled = false;
            this.Text = "Issuing Mode - Iso8583-1987";
            StopServer();
            StartServer();
        }

        private void iso85831993ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (mode != 5) Reset();
            mode = 5;
            ParseB.Enabled = false;
            this.Text = "Issuing Mode - Iso8583-1993";
            StopServer();
            StartServer();
        }

        private void bicIsoBase24ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (mode != 6) Reset();
            mode = 6;
            ParseB.Enabled = false;
            this.Text = "Issuing Mode - BicIso-Base24";
            StopServer();
            StartServer();
        }

        private void iso85831987ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (mode != 7) Reset();
            mode = 7;
            ParseB.Enabled = true;
            this.Text = "Parsing Mode - Iso8583-1987";
            StopServer();
        }

        private void iso85831993ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (mode != 8) Reset();
            mode = 8;
            ParseB.Enabled = true;
            this.Text = "Parsing Mode - Iso8583-1993";
            StopServer();
        }

        private void bicIsoBase24ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (mode != 9) Reset();
            mode = 9;
            ParseB.Enabled = true;
            this.Text = "Parsing Mode - BicIso-Base24";
            StopServer();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void configureCommToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            NetworkConfig nc = new TestParser.NetworkConfig();
            nc.FormClosed += other_Closed;
            nc.Show();
        }

        private void apiTransactionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            CoreApiTransactions nc = new TestParser.CoreApiTransactions();
            nc.FormClosed += other_Closed;
            nc.Show();
        }

        private void socketClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            SocketClient nc = new TestParser.SocketClient();
            nc.FormClosed += other_Closed;
            nc.Show();
        }
        #endregion

        #region Network
        private void StartServer()
        {
            try
            {
                // Creates one SocketPermission object for access restrictions
                permission = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, "", SocketPermission.AllPorts);

                // Listening Socket object 
                sListener = null;

                // Ensures the code to have permission to access a Socket 
                permission.Demand();

                // Resolves a host name to an IPHostEntry instance 
                IPHostEntry ipHost = Dns.GetHostEntry("");

                // Gets first IP address associated with a localhost 
                IPAddress ipAddr = ipHost.AddressList[0];

                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var cm = ((AppSettingsSection)config.GetSection("NetworkConfig")).Settings;
                string port = cm["port"].Value;

                // Creates a network endpoint 
                ipEndPoint = new IPEndPoint(ipAddr, int.Parse(port));

                // Create one Socket object to listen the incoming connection 
                sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Associates a Socket with a local endpoint 
                sListener.Bind(ipEndPoint);

                ReplyBox.Text = "Server started." + Environment.NewLine;
                
                // Places a Socket in a listening state and specifies the maximum 
                // Length of the pending connections queue 
                sListener.Listen(10);

                // Begins an asynchronous operation to accept an attempt 
                AsyncCallback aCallback = new AsyncCallback(AcceptCallback);
                
                sListener.BeginAccept(aCallback, sListener);
                
                ReplyBox.Text += "Server is now listening on " + ipEndPoint.Address + " port: " + ipEndPoint.Port;
            }
            catch (Exception ex)
            {
                ReplyBox.Text = ex.Message;
            }
        }

        private void StopServer()
        {
            try
            {
                if (sListener == null) return;
                if (sListener.Connected)
                {
                    sListener.Shutdown(SocketShutdown.Receive);
                    sListener.Close();
                }
                if (sListener.IsBound)
                {
                    sListener.Close();
                }
            }
            catch (Exception ex)
            {
                ReplyBox.Text = ex.Message;
            }
        }

        public void AcceptCallback(IAsyncResult ar)
        {
            Socket listener = null;

            // A new Socket to handle remote host communication 
            Socket handler = null;
            try
            {
                // Receiving byte array 
                byte[] buffer = new byte[4096];
                // Get Listening Socket object
                listener = (Socket)ar.AsyncState;

                // Create a new socket 
                handler = listener.EndAccept(ar);

                // Using the Nagle algorithm 
                handler.NoDelay = false;

                // Creates one object array for passing data 
                object[] obj = new object[2];
                obj[0] = buffer;
                obj[1] = handler;

                // Begins to asynchronously receive data 
                handler.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), obj);

                // Begins an asynchronous operation to accept an attempt 
                AsyncCallback aCallback = new AsyncCallback(AcceptCallback);
                listener.BeginAccept(aCallback, listener);
            }
            catch { }
        }

        public void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Fetch a user-defined object that contains information 
                object[] obj = new object[2];
                obj = (object[])ar.AsyncState;

                // Received byte array 
                byte[] buffer = (byte[])obj[0];

                // A Socket to handle remote host communication. 
                handler = (Socket)obj[1];

                // Received message 
                string content = string.Empty;
                
                // The number of bytes received. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    content += Encoding.Unicode.GetString(buffer, 0, bytesRead);
                    int length = int.Parse(content.Substring(0, content.IndexOf("ISO")), System.Globalization.NumberStyles.HexNumber);

                    if (content.Length > length)
                    {
                        // Convert byte array to string
                        string str = content.Substring(content.IndexOf("ISO"), length);

                        //this is used because the UI couldn't be accessed from an external Thread
                        this.BeginInvoke((ThreadStart)delegate ()
                        {
                            ReplyBox.Text = "Read " + str.Length * 2 + " bytes from client.\n Data: " + str;
                        });

                        if (mode % 3 == 1) // Iso8583 - 1987
                        {
                            Iso87Message Imsg = new Iso87Message(str);
                            Iso87Message Rmsg = Imsg.Reply();
                            Imsg.LinkedMessage = Rmsg;
                            this.BeginInvoke((ThreadStart)delegate ()
                            {
                                DisplayMessage(Imsg);
                            });
                            AllMessages.Add(Imsg);

                            string rep = Rmsg.ToReplyString();

                            // Prepare the reply message 
                            byte[] byteData = Encoding.Unicode.GetBytes(rep.Length.ToString("X") + rep);

                            // Sends data asynchronously to a connected Socket 
                            handler.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), handler);
                        }
                        else if (mode % 3 == 2) Display.Text = "Not Implemented yet"; // Iso8583 - 1993
                        else if (mode % 3 == 0) // BicIso - Base24
                        {
                            BicIsoMessage Imsg = new BicIsoMessage(str);
                            BicIsoMessage Rmsg = Imsg.Reply();
                            Imsg.LinkedMessage = Rmsg;
                            this.BeginInvoke((ThreadStart)delegate ()
                            {
                                DisplayMessage(Imsg);
                            });
                            AllMessages.Add(Imsg);

                            string rep = Rmsg.ToReplyString();

                            // Prepare the reply message 
                            byte[] byteData = Encoding.Unicode.GetBytes(rep.Length.ToString("X") + rep);

                            // Sends data asynchronously to a connected Socket 
                            handler.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), handler);
                        }
                        this.BeginInvoke((ThreadStart)delegate ()
                        {
                            UpdateMessages();
                        });
                    }
                }
                buffer = new byte[4096];
                handler.NoDelay = false;
                
                obj = new object[2];
                obj[0] = buffer;
                obj[1] = handler;
                handler.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), obj);
            }
            catch (Exception ex)
            {
                this.BeginInvoke((ThreadStart)delegate ()
                {
                    ReplyBox.Text = ex.Message;
                });
            }
        }

        public void SendCallback(IAsyncResult ar)
        {
            try
            {
                // A Socket which has sent the data to remote host 
                Socket handler = (Socket)ar.AsyncState;

                // The number of bytes sent to the Socket 
                int bytesSend = handler.EndSend(ar);
                this.BeginInvoke((ThreadStart)delegate ()
                {
                    ReplyBox.Text += String.Format("Sent {0} bytes to Client.", bytesSend);
                });
            }
            catch (Exception ex)
            {
                this.BeginInvoke((ThreadStart)delegate ()
                {
                    ReplyBox.Text = ex.Message;
                });
            }
        }
        #endregion
    }
}
