﻿using System;
using System.Configuration;
using System.Windows.Forms;

namespace TestParser
{
    public partial class NetworkConfig : Form
    {
        public NetworkConfig()
        {
            InitializeComponent();
            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var cm = ((AppSettingsSection)config.GetSection("NetworkConfig")).Settings;
                ipBox.Text = cm["ip"].Value;
                portBox.Text = cm["port"].Value;

            }
            catch { }
        }

        private void saveB_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ipBox.Text) || string.IsNullOrWhiteSpace(portBox.Text)) return;
            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var cm = ((AppSettingsSection)config.GetSection("NetworkConfig")).Settings;

                cm["ip"].Value = ipBox.Text;
                cm["port"].Value = portBox.Text;
            
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
            }
            catch { }
            this.Close();
        }

        private void cancelB_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
