﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace TestParser
{
    public partial class SocketClient : Form
    {
        // Receiving byte array  
        byte[] bytes = new byte[4096];
        Socket senderSock;

        public SocketClient()
        {
            InitializeComponent();
            button1.Enabled = true;
            button2.Enabled = false;
            button3.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e) // connect
        {
            try
            {
                // Create one SocketPermission for socket access restrictions 
                SocketPermission permission = new SocketPermission(NetworkAccess.Connect, TransportType.Tcp, "", SocketPermission.AllPorts);

                // Ensures the code to have permission to access a Socket 
                permission.Demand();
                
                // Get ip and port from network configuration 
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var cm = ((AppSettingsSection)config.GetSection("NetworkConfig")).Settings;
                string ip = cm["ip"].Value;
                string port = cm["port"].Value;
                IPAddress ipAddr = IPAddress.Parse(ip);

                // Creates a network endpoint 
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, int.Parse(port));

                // Create one Socket object to setup Tcp connection 
                senderSock = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                senderSock.NoDelay = false;   // Using the Nagle algorithm 

                // Establishes a connection to a remote host 
                senderSock.Connect(ipEndPoint);
                ReplyBox.Text = "Socket connected to " + senderSock.RemoteEndPoint.ToString();

                button1.Enabled = false;
                button2.Enabled = true;
                button3.Enabled = true;
            }
            catch (Exception ex)
            {
                ReplyBox.Text = ex.Message;
            }
        }

        private void button2_Click(object sender, EventArgs e) // send
        {
            try
            {
                ReplyBox.Text = "";

                // Sending message 
                //  sending 2 bytes (hex) of the message length
                string theMessageToSend = MessageBox.Text;
                byte[] msg = Encoding.Unicode.GetBytes(theMessageToSend.Length.ToString("X") + theMessageToSend);

                // Sends data to a connected Socket. 
                int bytesSend = senderSock.Send(msg);

                // Receives data from a bound Socket. 
                int bytesRec = senderSock.Receive(bytes);

                // Converts byte array to string 
                String theMessageToReceive = Encoding.Unicode.GetString(bytes, 0, bytesRec);

                // Continues to read the data till data isn't available 
                while (senderSock.Available > 0)
                {
                    bytesRec = senderSock.Receive(bytes);
                    theMessageToReceive += Encoding.Unicode.GetString(bytes, 0, bytesRec);
                }

                ReplyBox.Text = "The server reply: " + theMessageToReceive;
            }
            catch (Exception ex)
            {
                ReplyBox.Text = ex.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e) // disconnect
        {
            try
            {
                // Disables sends and receives on a Socket. 
                senderSock.Shutdown(SocketShutdown.Both);

                //Closes the Socket connection and releases all resources 
                senderSock.Close();

                button1.Enabled = true;
                button2.Enabled = false;
                button3.Enabled = false;
            }
            catch (Exception ex)
            {
                ReplyBox.Text = ex.Message;
            }
        }
    }
}
